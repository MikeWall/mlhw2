from sklearn import datasets
import numpy as np

cancer = datasets.load_breast_cancer()

X = cancer.data
y = cancer.target

labels = np.unique(y)

# print('Features:\n', cancer.feature_names)
# print('Targets:\n', cancer.target_names)
# print('Class labels:', labels)
# print(cancer.DESCR)

from sklearn.cross_validation import train_test_split

X_train, X_test, y_train, y_test = \
  train_test_split(X, y, test_size=0.3, random_state=0)


# Let's convert the data from a numpy array to
# a pandas dataframe and dump the summary statistics.

import pandas as pd
pd.set_option('precision', 1)

df = pd.DataFrame(data=cancer.data)
df.describe()

from sklearn.preprocessing import StandardScaler

sc = StandardScaler()
sc.fit(X_train)
X_train_std = sc.transform(X_train)
X_test_std  = sc.transform(X_test)

df = pd.DataFrame(X_train_std)
df.describe()


from sklearn.svm import SVC

# svm = SVC(kernel='rbf', C=1.0, random_state=0)
# svm = SVC(kernel='rbf', C=100.0, gamma=0.001, random_state=0)

#import GridSearch
from sklearn.grid_search import GridSearchCV
import sklearn.metrics as scores
tuned_parameters = [
{'kernel': ['linear'], 'C': range(1, 1000)},
{'kernel': ['rbf'], 'gamma': [1, 1e-1, 1e-2, 1e-3, 1e-4], 'C': range(1, 500)}]
{'kernel': ['poly'], 'C': range(1, 1000)},
{'kernel': ['sigmoid'], 'gamma': [1e-3, 1e-4], 'C': range(1, 1000)}]

                    # {'kernel': ['linear'], 'C': [1, 10, 100, 1000]},
                    # {'kernel': ['rbf'], 'gamma': [1e-3, 1e-4], 'C': [1, 10, 100, 1000]},
                    # {'kernel': ['poly'], 'C': [1, 10, 100, 1000]}]
                    # {'kernel': ['sigmoid'], 'gamma': [1e-3, 1e-4], 'C': [1, 10, 100, 1000]}]
svm = GridSearchCV(SVC(), tuned_parameters, scoring='recall_weighted')

svm.fit(X_train_std, y_train)

# print("Best parameters set found on development set:")
# print()
# print(svm.best_params_)
# print()
# for params, mean_score, scores in svm.grid_scores_:
#     print("%0.3f (+/-%0.03f) for %r" % (mean_score, scores.std() * 2, params))
# print()

X_combined_std = np.vstack((X_train_std, X_test_std))
y_combined = np.hstack((y_train, y_test))

ntest, nfeatures  = X_train_std.shape
ntotal, nfeatures = X_combined_std.shape


from sklearn import metrics

np.set_printoptions(precision=6)

y_pred = svm.predict(X_train_std)
cm = metrics.confusion_matrix(y_train, y_pred)
# print('Confusion matrix for the training set, without normalization')
# print(cm)

# Normalize the confusion matrix by row (i.e by the number of samples
# in each class)
cm_normalized = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
print('Normalized confusion matrix for the training set')
print(cm_normalized)

print('Precision, recall, and F-score for the training set')
print(metrics.classification_report(y_train, y_pred))


from sklearn import metrics

# Use the classifier to make predictions for the test set
y_pred = svm.predict(X_test_std)

# Compute confusion matrix
cm = metrics.confusion_matrix(y_test, y_pred)
print('Confusion matrix for the test set, without normalization')
print(cm)

# Normalize the confusion matrix by row (i.e by the number of samples
# in each class)
cm_normalized = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
print('Normalized confusion matrix for the test set')
print(cm_normalized)

print('Precision, recall, and F-score for the test set')
print(metrics.classification_report(y_test, y_pred))


# %matplotlib inline
import matplotlib.pyplot as plt

def plot_confusion_matrix(cm, title='Confusion matrix', cmap=plt.cm.Blues):
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(cancer.target_names))
    plt.xticks(tick_marks, cancer.target_names, rotation=45)
    plt.yticks(tick_marks, cancer.target_names)
    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')

plt.figure()
plot_confusion_matrix(cm)

plt.figure()
plot_confusion_matrix(cm_normalized, title='Normalized confusion matrix')
